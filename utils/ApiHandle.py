import json
from os import path

import requests

BASE_URL = 'https://spring-boot-university.herokuapp.com'
PATH_API = '/_api/v1/sms'


def login():
    url = BASE_URL + '/login'
    headers = {'content-type': 'application/json'}
    data = json.dumps({'username': 'admin', 'password': 'admin'})
    response = requests.post(url=url, data=data, headers=headers)
    writeToken(response.json()['credentialDTO']['accessToken'])


def sendSMS(student):
    url = BASE_URL + PATH_API + '/sendSMS'
    if path.exists("token.txt") is False:
        login()
    token = readToken()
    headers = {'content-type': 'application/json', 'Authorization': 'Bearer ' + token}
    if int(student.percent) <= 20:
        message = 'Sinh viên ' + student.fullName + ' đã nghỉ ' + '( ' + str(round(student.percent)) + '%| 20 %)'
        data = json.dumps({'phone': '+84' + str(int(student.parentPhone)), 'message': message})
        response = requests.post(url=url, data=data, headers=headers)
        print(response.json())
    elif int(student.percent) > 20:
        message = 'Sinh viên ' + student.fullName + ' đã nghỉ quá số buổi quy định ' + '( ' + str(
            round(student.percent)) + '%| 20%)'
        data = json.dumps({'phone': '+84' + str(int(student.parentPhone)), 'message': message})
        response = requests.post(url=url, data=data, headers=headers)
        print(response.json())


def writeToken(token):
    f = open("token.txt", "w+")
    f.write(token)
    f.close()


def readToken():
    f = open("token.txt", "r")
    content = f.read()
    return content
