import psycopg2
from datetime import datetime, timezone

import pytz

# Param ( Change Every week with PostgreSQL Heroku ( Free version ) )
HOST_URL = "ec2-54-83-33-14.compute-1.amazonaws.com"
PATH_DEFINE = "?&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory"
PATH_DEFINE2 = "&useUnicode=yes&characterEncoding=UTF-8&serverTimezone=UTC"
USERNAME = "cirgaznbsyyrmy"
PASSWORD = "0128127a1ace3127ba006a2c912c8fcab42c0b95ffe4ea37e8ad2ed1256e88a0"
PORT = "5432"
DATABASE = "denol0hoq3dveu"


# Create connect to PostgreSQL
def connectSQL():
    try:
        connection = psycopg2.connect(user=USERNAME, password=PASSWORD, host=HOST_URL, port=PORT, database=DATABASE)
        cursor = connection.cursor()
        # Print PostgreSQL Connection properties
        print(connection.get_dsn_parameters(), "\n")
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        print("You are connected to - ", record, "\n")
        return connection
    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)
        return None


# Get all record in student table when training_status is 0
def getAllStudent():
    conn = None
    query = "SELECT roll_number, image FROM student WHERE training_status=0"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query)
        results = cursor.fetchall()
        print("The number of parts: ", cursor.rowcount)
        # for result in results:
        #     print(result)
        cursor.close()
        return results
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


# After trained success. Change trainingStatus to 1
# and save student
def updateStudent(rollNumber):
    conn = None
    query = "Update student set training_status = %s where roll_number = %s"
    query1 = "select * from student where roll_number = %s"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query, (1, rollNumber))
        conn.commit()
        count = cursor.rowcount
        print(count, "Record Updated successfully ")

        print("Table After updating record ")
        cursor.execute(query1, (rollNumber,))
        record = cursor.fetchone()
        print(record)
        cursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error in update operation", error)
    finally:
        if conn is not None:
            conn.close()
            print("PostgreSQL connection is closed")


# Get schedule when start
def getScheduleWhenStart():
    currentDate = datetime.today().strftime('%Y-%m-%d')
    conn = None
    query = "SELECT * FROM schedule where date <= %s and status = %s"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query, (currentDate, 1))
        results = cursor.fetchall()
        print("The number of parts: ", cursor.rowcount)
        cursor.close()
        return results
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def updateSchedule(id):
    conn = None
    query = "Update schedule set status = %s where id = %s"
    query1 = "select * from schedule where id = %s"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query, (14, id))
        conn.commit()
        count = cursor.rowcount
        print(count, "Record Updated successfully ")

        print("Table After updating record ")
        cursor.execute(query1, (id,))
        record = cursor.fetchone()
        print(record)
        cursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error in update operation", error)
    finally:
        if conn is not None:
            conn.close()
            print("PostgreSQL connection is closed")


# Insert Attendance when student attended
def addAttendence(scheduleId, studentId, status):
    tz_VN = pytz.timezone('Asia/Ho_Chi_Minh')
    currentDateTime = datetime.now().replace(microsecond=0).replace(tzinfo=timezone.utc).astimezone(
        tz=tz_VN).timestamp()
    conn = None
    query = "INSERT INTO attendance(created_at, updated_at, status, deleted_at, schedule_id, student_id) values (%s, %s, %s, %s, %s, %s)"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query, (currentDateTime, currentDateTime, status, 0, scheduleId, studentId))
        conn.commit()
        count = cursor.rowcount
        print(count, "Record inserted successfully into attendance")
        print("Create attendence success")
        cursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def getSlotById(id):
    conn = None
    query = "Select start_time, end_time from slot where id = %s"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query, (id,))
        result = cursor.fetchone()
        print(result)
        return result
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def getStudentByClass(classId):
    conn = None
    query = "SELECT student.id, student.roll_number, student.phone, student.parent_phone, student.full_name, class.id, class.name" \
            " from student" \
            " inner join student_class" \
            " on student.id = student_class.student_id" \
            " inner join class" \
            " on student_class.class_id = class.id" \
            " where student_class.class_id = %s"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query, (classId,))
        result = cursor.fetchall()
        print(result)
        return result
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def getAllEmployee():
    conn = None
    query = "Select * From employee"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        print(result)
        return result
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def getEmailFromAccountId(accountId):
    conn = None
    query = "Select email From account where id= %s"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query, (accountId,))
        result = cursor.fetchone()
        print(result)
        return result
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def getAllScheduleByClassAndSubject(classId, subjectId):
    conn = None
    query = "Select * From schedule where clazz_id= %s and subject_id= %s"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query, (classId, subjectId,))
        result = cursor.fetchall()
        print(result)
        return result
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def getAttendanceByStudentAndStatusAndSchedule(studentId, scheduleId):
    conn = None
    query = "Select * From attendance where student_id= %s and schedule_id= %s and status= %s"
    try:
        conn = connectSQL()
        cursor = conn.cursor()
        cursor.execute(query, (studentId, scheduleId, 12,))
        result = cursor.fetchone()
        print(result)
        return result
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
