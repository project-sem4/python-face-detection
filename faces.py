import threading

import numpy as np
import cv2
import pickle
from datetime import datetime
import pytz
from playsound import playsound

listStudent = []


def attendenceFaceDetect(endTime):
    face_cascade = cv2.CascadeClassifier('cascades/data/haarcascades/haarcascade_frontalface_alt2.xml')
    recognizer = cv2.face.LBPHFaceRecognizer_create()
    recognizer.read("trainer.yml")

    labels = {"person_name": 1}
    with open("labels.pickle", "rb") as f:
        og_labels = pickle.load(f)
        labels = {v: k for k, v in og_labels.items()}

    cap = cv2.VideoCapture(0)
    while True:
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, scaleFactor=1.5, minNeighbors=5)
        for (x, y, w, h) in faces:
            print(x, y, w, h)
            roi_gray = gray[y:y + h, x:x + w]

            id_, conf = recognizer.predict(roi_gray)
            if 45 <= conf:
                print(id_)
                print(labels[id_])
                font = cv2.FONT_HERSHEY_COMPLEX_SMALL
                name = labels[id_]
                color = (255, 255, 255)
                stroke = 2
                cv2.putText(frame, name, (x, y), font, 1, color, cv2.LINE_4)

            img_item = "my-image.png"
            cv2.imwrite(img_item, roi_gray)

            color = (255, 0, 0)
            stroke = 2
            end_cord_x = x + w
            end_cord_y = y + h
            cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
            sound_thread = threading.Thread(target=lambda: playsound('sound.mp3'))
            sound_thread.start()
            listStudent.append(labels[id_])
        cv2.imshow('frame', frame)
        endTimeSplit = endTime.split(':')
        tz_VN = pytz.timezone('Asia/Ho_Chi_Minh')
        datetime_VN = datetime.now(tz_VN)
        currentTime = datetime_VN.strftime("%H:%M")
        endTimeConvert = datetime.now().replace(hour=int(endTimeSplit[0]), minute=int(endTimeSplit[1])).strftime(
            "%H:%M")
        if cv2.waitKey(20) & 0xFF == ord('q'):
            break
        if currentTime > endTimeConvert:
            break

    cap.release()
    cv2.destroyAllWindows()
    return listStudent
