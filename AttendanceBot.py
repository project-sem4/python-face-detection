from datetime import datetime, timezone
import time

import pytz

import utils.SQLHandle as sqlH
import schedule

from Util import percentCalculator
from Util import sendEmail
from entity.Student import Student
from faces import attendenceFaceDetect
from utils.ApiHandle import sendSMS


def sendNotification(classId, subjectId):
    listEmail = []
    listSchedule = sqlH.getAllScheduleByClassAndSubject(classId, subjectId)
    listAbsentAttendance = []
    listStudentInClass = sqlH.getStudentByClass(classId)
    list = sqlH.getAllEmployee()
    listabsentStudent = []
    mySet1 = set()
    mySet2 = set()
    for e in list:
        print(e[9])
        email = sqlH.getEmailFromAccountId(e[9])
        listEmail.extend(email)
    for st in listStudentInClass:
        for sche1 in listSchedule:
            absentAt = sqlH.getAttendanceByStudentAndStatusAndSchedule(st[0], sche1[0])
            if absentAt is not None:
                listAbsentAttendance.append(absentAt)
        if len(listAbsentAttendance) > 0:
            percent = percentCalculator(len(listAbsentAttendance), len(listSchedule))
            student = Student(id=st[0], rollNumber=st[1], fullName=st[4], phone=st[2],
                              parentPhone=st[3],
                              clazz=st[6], percent=percent)
            sendSMS(student)
            listabsentStudent.append(student)
            mySet1 = set(listabsentStudent)
            mySet2 = set(listEmail)
    sendEmail(mySet1, mySet2)


def job():
    print("I'm working...")
    thislist = sqlH.getScheduleWhenStart()
    subjectId = 0
    classId = 0
    for sche in thislist:
        print(sche)
        print(sche[10])
        subjectId = sche[11]
        classId = sche[6]
        resultSlot = sqlH.getSlotById(sche[10])
        print(resultSlot)
        tz_VN = pytz.timezone('Asia/Ho_Chi_Minh')
        datetime_VN = datetime.now(tz_VN)
        currentDateTime = datetime_VN.strftime("%Y-%m-%d %H:%M")
        date = sche[2]
        splitEndtime = resultSlot[1].split(':')
        endTimeConvert = datetime_VN.replace(hour=int(splitEndtime[0]), minute=int(splitEndtime[1])).strftime("%H:%M")
        if currentDateTime < date + ' ' + endTimeConvert:
            thisList = attendenceFaceDetect(resultSlot[1])
            mySet = set(thisList)
            for st in sqlH.getStudentByClass(classId):
                if st[1] in mySet:
                    print(st[1].lower())
                    print("Match value" + " " + st[1])
                    sqlH.addAttendence(sche[0], st[0], 13)
                else:
                    print("Not Exist value" + " " + st[1])
                    sqlH.addAttendence(sche[0], st[0], 12)
            sqlH.updateSchedule(sche[0])
            time.sleep(10)
            sendNotification(classId, subjectId)
        else:
            for st in sqlH.getStudentByClass(sche[6]):
                sqlH.addAttendence(sche[0], st[0], 12)
            sqlH.updateSchedule(sche[0])
            time.sleep(10)
            sendNotification(classId, subjectId)


schedule.every(1).minutes.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
