import os
import re

import schedule
import time

import Util
import utils.SQLHandle as sqlH


def job():
    print("I'm working....")
    listStudent = sqlH.getAllStudent()

    for st in listStudent:
        print(st)
        print(st[1])
        splitImage = re.split(',', st[1])
        for x in splitImage:
            Util.downloadImage(x, st[0])
        print("I'm downloading and creating folder done.")
        sqlH.updateStudent(st[0])
        print("I'm changing training status")

    print("Waiting...............")
    print("I'm training data")
    os.system("python face-training.py")


schedule.every(1).minutes.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
