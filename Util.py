import errno
import os
import smtplib
import unicodedata

import requests

from entity.Student import Student


def downloadImage(url, rollNumber):
    stringPath = "images" + "/" + rollNumber
    count = 1
    try:
        # Create new Folder by Roll Number
        os.makedirs(stringPath)
        print("create folder success")
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    finally:
        # After download check file in dir
        list = os.listdir(stringPath)
        number_files = len(list)
        print(number_files)
        count = number_files
        count = count + 1
        # After create new Folder, download image.
        response = requests.get(url)
        if response.status_code == 200:
            with open(stringPath + '/' + str(count) + '.jpg', 'wb') as f:
                f.write(response.content)
                f.close()
        print("Download and save image success")
        return


def sendEmail(listStudent, listEmployeeEmail):
    gmail_user = 'domain9421102@gmail.com'
    gmail_password = 'akdqsqspqnaqxqxk'
    print(listStudent)
    print(listEmployeeEmail)
    sent_from = gmail_user
    to = listEmployeeEmail
    subject = 'List Student Absent'
    content = '============================================== \n'
    content += '{0} {1:>5} {2:>5} {3:>5} {4:>5} {5:>5} \n'.format('|', 'Roll Number', '|', 'Full Name', '|',
                                                                  'Percent')
    content += '============================================== \n'
    for st in listStudent:
        fName = str(st.fullName).replace('Đ', 'D').replace('đ', 'd').replace('ê', 'e').replace('Ê', 'E')
        fullName = unicodedata.normalize('NFKD', fName).encode('ascii', 'ignore')
        sPercent = str(round(st.percent)) + '% / 20%'
        content += '{0} {1:>5} {2:>5} {3:>5} {4:>5} {5:>5} \n'.format('|', str(st.rollNumber), '|',
                                                                      fullName.decode('utf-8'),
                                                                      '|', sPercent)
        content += '============================================== \n'

    body = content

    message = 'Subject: {}\n\n{}'.format(subject, body)

    try:
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        server.sendmail(sent_from, to, message)
        server.close()
        print('Email sent!')
    except Exception as e:
        print('Something went wrong...' + str(e))


def percentCalculator(numberOfAttendance, numberOfSchedule):
    if numberOfSchedule == 0:
        print(ZeroDivisionError)
    else:
        return (numberOfAttendance / numberOfSchedule) * 100
