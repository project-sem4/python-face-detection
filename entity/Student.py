class Student(object):

    def __init__(self, id, fullName, phone, parentPhone, rollNumber, percent, clazz) -> None:
        self.id = id
        self.fullName = fullName
        self.phone = phone
        self.parentPhone = parentPhone
        self.rollNumber = rollNumber
        self.percent = percent
        self.clazz = clazz
